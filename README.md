# OpenML dataset: qqdefects_numeric

https://www.openml.org/d/1072

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.csv`](./dataset/tables/data.csv): CSV file with data
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

**Author**:   
**Source**: Unknown - Date unknown  
**Please cite**:   

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
This is a PROMISE Software Engineering Repository data set made publicly
available in order to encourage repeatable, verifiable, refutable, and/or
improvable predictive models of software engineering.

If you publish material based on PROMISE data sets then, please
follow the acknowledgment guidelines posted on the PROMISE repository
web page http://promisedata.org/repository .
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
(c) 2007  Norman Fenton
This data set is distributed under the
Creative Commons Attribution-Share Alike 3.0 License
http://creativecommons.org/licenses/by-sa/3.0/

You are free:

* to Share -- copy, distribute and transmit the work
* to Remix -- to adapt the work

Under the following conditions:

Attribution. You must attribute the work in the manner specified by
the author or licensor (but not in any way that suggests that they endorse
you or your use of the work).

Share Alike. If you alter, transform, or build upon this work, you
may distribute the resulting work only under the same, similar or a
compatible license.

* For any reuse or distribution, you must make clear to others the
license terms of this work.
* Any of the above conditions can be waived if you get permission from
the copyright holder.
* Apart from the remix rights granted under this license, nothing in
this license impairs or restricts the author's moral rights.

Qualitative and quantitative data about 31 projects completed in a
consumer electronics company (one row per project).
There
is a mixture of qualitative attributes (these are measured on a 5
point ranked scale VL, L, M, H, VH) and quantitative attributes
whose scale is stated.
from..
Title:
Project Data Incorporating Qualitative Factors for Improved Software Defect;
Author(s):
Norman Fenton and Martin Neil and William Marsh and Peter Hearty and Lukasz Radlinski and Paul Krause;
Published in:
in Proceedings of the PROMISE workshop;
Year:
2007;
attributes:
S1	Relevant Experience of Spec & Doc Staff
S2	Quality of Documentation inspected
S3	Regularity of Spec & Doc Reviews
S4	Standard Procedures Followed
S5	Quality of Documentation inspected
S6	Spec Defects Discovered in Review
S7	Requirements Stability
F1	Complexity of new functionality
F2	Scale of New functionality implemented
F3	Total no. of Inputs and Outputs
D1	Relevant Development Staff Experience
D2	Programmer capability
D3	Defined processes followed
D4	Development Staff motivation
T1	Testing Process Well Defined
T2	Testing Staff Experience
T3	Testing Staff Experience
T4	Quality of Documented Test Cases
P1	Dev. Staff Training Quality
P2	Requirements Management
P3	Project Planning
P4	Scale of Distributed Communication
P5	Stake-holder involvement
P6	Stake-holder involvement
P7	Vendor Management
P8	Internal communication/interaction
P9	Process Maturity
E	Total Effort
K	KLOC
L	Language
TD	Testing Defects (Pre+ Post)

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/1072) of an [OpenML dataset](https://www.openml.org/d/1072). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/1072/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/1072/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/1072/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

